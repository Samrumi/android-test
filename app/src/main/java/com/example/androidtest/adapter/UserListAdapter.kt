package com.example.androidtest.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtest.R
import com.example.androidtest.network.response.UserData
import com.example.androidtest.ui.UserDetailsActivity
import com.example.androidtest.utils.loadImage


class UserListAdapter(private val items: ArrayList<UserData>) :
	RecyclerView.Adapter<UserListAdapter.ViewHolder>(), Filterable {

	private var filteredList = items
	private var context: Context ?= null

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(filteredList[position])
		holder.itemView.setOnClickListener {
			val intent = Intent(context, UserDetailsActivity::class.java)
			intent.putExtra("data", filteredList[position])
			context?.startActivity(intent)
		}
	}

	override fun getItemCount(): Int {
		return filteredList.size
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		context = parent.context
		return ViewHolder.from(parent)
	}
	
	class ViewHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
		private val name = view.findViewById<AppCompatTextView>(R.id.tv_user_name)
		private val image = view.findViewById<AppCompatImageView>(R.id.iv_user)

		fun bind(item: UserData) {
			name.text = item.login
			image.loadImage(item.avatarUrl)
		}
		
		companion object {
			fun from(parent: ViewGroup): ViewHolder {
				val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user,
						parent, false)
				return ViewHolder(view)
			}
		}
	}

	override fun getFilter(): Filter {
		return object : Filter() {
			override fun performFiltering(charSequence: CharSequence): FilterResults {
				val charString = charSequence.toString()
				filteredList = if (charString.isEmpty()) {
					items
				} else {
					val newFilteredList = ArrayList<UserData>()
					for (row in items) {
						if (row.login.toLowerCase().contains(charString.toLowerCase())) {
							newFilteredList.add(row)
						}
					}
					newFilteredList
				}

				val filterResults = FilterResults()
				filterResults.values = filteredList
				return filterResults
			}

			override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
				filteredList = filterResults.values as ArrayList<UserData>
				notifyDataSetChanged()
			}
		}
	}
}

