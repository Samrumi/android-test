package com.example.androidtest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androidtest.R


class UserDetailAdapter(private val items: ArrayList<String>) :
	RecyclerView.Adapter<UserDetailAdapter.ViewHolder>() {

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(items[position], position)
	}

	override fun getItemCount(): Int {
		return items.size
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		return ViewHolder.from(parent)
	}
	
	class ViewHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
		private val name = view.findViewById<AppCompatTextView>(R.id.tv_detail_name)
		private val info = view.findViewById<AppCompatTextView>(R.id.tv_detail_info)

		fun bind(item: String, position: Int) {
			when(position){
				0 -> setInfo("Name:", item)
				1 -> setInfo("Followers:", item)
				2 -> setInfo("Following:", item)
				3 -> setInfo("Gists:", item)
				4 -> setInfo("Subscriptions:", item)
				5 -> setInfo("Organizations:", item)
				6 -> setInfo("Repos:", item)
			}
		}

		private fun setInfo(menuName: String, menuData: String){
			name.text = menuName
			info.text = menuData
		}
		
		companion object {
			fun from(parent: ViewGroup): ViewHolder {
				val view = LayoutInflater.from(parent.context).inflate(R.layout.item_detail,
						parent, false)
				return ViewHolder(view)
			}
		}
	}

}

