package com.example.androidtest.repository

import androidx.lifecycle.LiveData
import com.example.androidtest.data.AppDataSource
import com.example.androidtest.network.Result
import com.example.androidtest.network.response.UserData

class AppRepositoryImpl(private val dataSource: AppDataSource) : AppRepository {

    override fun getUsers(): LiveData<Result<ArrayList<UserData>>> {
        dataSource.getUsers()
        return dataSource.fetchedUserList
    }

}