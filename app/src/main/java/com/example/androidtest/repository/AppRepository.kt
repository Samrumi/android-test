package com.example.androidtest.repository

import androidx.lifecycle.LiveData
import com.example.androidtest.network.Result
import com.example.androidtest.network.response.UserData

interface AppRepository {
    fun getUsers(): LiveData<Result<ArrayList<UserData>>>
}
