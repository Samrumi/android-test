package com.example.androidtest

import android.app.Application
import com.example.androidtest.data.AppDataSource
import com.example.androidtest.data.AppDataSourceImpl
import com.example.androidtest.network.GithubApiService
import com.example.androidtest.network.utils.NetworkUtils
import com.example.androidtest.network.utils.NetworkUtilsImpl
import com.example.androidtest.repository.AppRepository
import com.example.androidtest.repository.AppRepositoryImpl
import com.example.androidtest.ui.MainVMFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class App : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@App))

        bind<NetworkUtils>() with singleton { NetworkUtilsImpl(instance()) }

        //api
        bind() from singleton { GithubApiService() }

        //data source
        bind<AppDataSource>() with singleton {
            AppDataSourceImpl(instance(), instance())
        }

        //repository
        bind<AppRepository>() with singleton { AppRepositoryImpl(instance()) }

        //view model factory
        bind() from provider { MainVMFactory(instance()) }

    }

}