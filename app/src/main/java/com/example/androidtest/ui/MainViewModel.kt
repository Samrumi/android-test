package com.example.androidtest.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.androidtest.network.Result
import com.example.androidtest.network.response.UserData
import com.example.androidtest.repository.AppRepository

class MainViewModel(private val repository: AppRepository) : ViewModel(){

    fun getUsersList(): LiveData<Result<ArrayList<UserData>>> {
        return repository.getUsers()
    }

}