package com.example.androidtest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidtest.R
import com.example.androidtest.adapter.UserDetailAdapter
import com.example.androidtest.network.response.UserData
import com.example.androidtest.utils.loadImage
import kotlinx.android.synthetic.main.activity_user_details.*

class UserDetailsActivity : AppCompatActivity() {

    private val dataList = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)

        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            finish()
        }

        val userData = intent?.extras?.getParcelable<UserData>("data")
        userData?.avatarUrl?.let { iv_user_detail.loadImage(it) }

        dataList.add(userData?.login.toString())
        dataList.add(userData?.followingUrl.toString())
        dataList.add(userData?.followersUrl.toString())
        dataList.add(userData?.gistsUrl.toString())
        dataList.add(userData?.subscriptionsUrl.toString())
        dataList.add(userData?.organizationsUrl.toString())
        dataList.add(userData?.reposUrl.toString())

        val userDetailAdapter = UserDetailAdapter(dataList)
        rv_user_detail.adapter = userDetailAdapter

    }
}
