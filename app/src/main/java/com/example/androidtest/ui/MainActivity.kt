package com.example.androidtest.ui

import android.app.SearchManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.androidtest.R
import com.example.androidtest.adapter.UserDetailAdapter
import com.example.androidtest.adapter.UserListAdapter
import com.example.androidtest.network.Result
import com.example.androidtest.network.response.UserData
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance

class MainActivity : AppCompatActivity(), KodeinAware {
    override val kodein: Kodein by closestKodein()
    private val mainVMFactory: MainVMFactory by instance()

    private var userList = ArrayList<UserData>()
    private var userAdapter: UserListAdapter? = null

    private val viewModel by lazy {
        ViewModelProviders.of(this, mainVMFactory).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        userAdapter = UserListAdapter(userList)
        rvUserList.adapter = userAdapter

        viewModel.getUsersList().observe(this, Observer {
            when (it) {
                is Result.Success -> {
                    userList.clear()
                    userList.addAll(it.data)
                    userAdapter?.notifyDataSetChanged()
                }
                is Result.Error -> {
                    Toast.makeText(this, it.exception, Toast.LENGTH_SHORT).show()
                }
            }
            progressBar.visibility = View.GONE
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView

        val searchIcon = searchView.findViewById<ImageView>(androidx.appcompat.R.id.search_button)
        searchIcon.setImageDrawable(
            ContextCompat.getDrawable(this, R.drawable.ic_search)
        )
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                userAdapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                userAdapter?.filter?.filter(query)
                return false
            }
        })
        return true
    }
}
