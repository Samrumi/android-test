package com.example.androidtest.network

import com.example.androidtest.network.response.UserData
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface GithubApiService {

    //users
    @GET("users")
    fun getUsers(): Single<ArrayList<UserData>>

    companion object {
        operator fun invoke() : GithubApiService {
            return Retrofit.Builder()
                .baseUrl(AppEndPoint.PRODUCTION.getBaseUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(GithubApiService::class.java)
        }
    }
}