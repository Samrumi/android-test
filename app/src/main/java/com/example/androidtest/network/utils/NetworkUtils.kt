package com.example.androidtest.network.utils

import com.example.androidtest.network.Result
import io.reactivex.Single

interface NetworkUtils {
    fun isNetworkConnected(): Boolean
    fun <T : Any> safeApiCall(call: () -> Single<T>, result: (Result<T>) -> Unit)
}
