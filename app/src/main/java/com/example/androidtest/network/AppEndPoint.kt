package com.example.androidtest.network

const val BASE_URL = "https://api.github.com/"

/*
    Not suitable for this example
 */
enum class AppEndPoint(private val endPoint: String) {
    //For Test Environment (/api/test/v1)
    TESTING(endPoint = ""),

    //For Production Environment (/api/v1)
    PRODUCTION(endPoint = "");

    fun getBaseUrl(): String {
        return BASE_URL + endPoint
    }

}
