package com.example.androidtest.utils

import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide

fun AppCompatImageView.loadImage(source: Any){
    Glide.with(context).load(source).into(this)
}