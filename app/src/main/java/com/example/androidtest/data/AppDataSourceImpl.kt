package com.example.androidtest.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.androidtest.network.GithubApiService
import com.example.androidtest.network.Result
import com.example.androidtest.network.response.UserData
import com.example.androidtest.network.utils.NetworkUtils

class AppDataSourceImpl(
    private val apiService: GithubApiService,
    private val networkUtils: NetworkUtils) : AppDataSource {

    private var _fetchedUserList: MutableLiveData<Result<ArrayList<UserData>>> = MutableLiveData()

    override fun getUsers() {
        networkUtils.safeApiCall({ apiService.getUsers() }, {
            _fetchedUserList.value = it
        })
    }

    override val fetchedUserList: LiveData<Result<ArrayList<UserData>>>
        get() = _fetchedUserList
}