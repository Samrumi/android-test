package com.example.androidtest.data

import androidx.lifecycle.LiveData
import com.example.androidtest.network.Result
import com.example.androidtest.network.response.UserData

interface AppDataSource {
    val fetchedUserList: LiveData<Result<ArrayList<UserData>>>
    fun getUsers()
}